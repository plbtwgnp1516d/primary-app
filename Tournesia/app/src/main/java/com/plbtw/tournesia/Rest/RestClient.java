package com.plbtw.tournesia.Rest;

/**
 * Created by Yoshua on 4/21/2016.
 */

import com.plbtw.tournesia.Helper.ToStringConverter;
import com.plbtw.tournesia.Model.APIBaseResponse;
import com.plbtw.tournesia.Model.APIHistory;
import com.plbtw.tournesia.Model.APIKonten;
import com.plbtw.tournesia.Model.APIUserData;
import com.plbtw.tournesia.Model.APIVotes;
import com.squareup.okhttp.Interceptor;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Response;

import java.io.IOException;

import retrofit.Call;
import retrofit.GsonConverterFactory;
import retrofit.Retrofit;
import retrofit.http.Field;
import retrofit.http.FormUrlEncoded;
import retrofit.http.GET;
import retrofit.http.Headers;
import retrofit.http.POST;
import retrofit.http.Query;

public class RestClient {

    private static GitApiInterface gitApiInterface ;
    private static String baseUrl = "http://hnwtvc.com" ;

    public static GitApiInterface getClient() {
        if (gitApiInterface == null) {

            OkHttpClient okClient = new OkHttpClient();
            okClient.interceptors().add(new Interceptor() {
                @Override
                public Response intercept(Chain chain) throws IOException {
                    Response response = chain.proceed(chain.request());
                    return response;
                }
            });

            Retrofit client = new Retrofit.Builder()
                    .baseUrl(baseUrl)
                    .addConverter(String.class, new ToStringConverter())
                    .client(okClient)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
            gitApiInterface = client.create(GitApiInterface.class);
        }
        return gitApiInterface ;
    }

    public interface GitApiInterface {

        @Headers("Cache-Control: no-cache")
        @GET("/tournesia-rest/index.php/APIv1/tournesia/kontenAll")
        Call<APIKonten> getKonten(@Query("email") String email);

        @Headers("Cache-Control: no-cache")
        @GET("/tournesia-rest/index.php/APIv1/tournesia/kontenHot")
        Call<APIKonten> getKontenHot(@Query("email") String email);

        @GET("/tournesia-rest/index.php/APIv1/tournesia/totalVotes")
        Call<APIVotes> getVotes(@Query("email") String email);

        @Headers("Cache-Control: no-cache")
        @GET("/tournesia-rest/index.php/APIv1/tournesia/getHistory")
        Call<APIHistory> getHistory(@Query("email") String email);

        @FormUrlEncoded
        @POST("/tournesia-rest/index.php/APIv1/tournesia/signUp")
        Call<APIBaseResponse> signUp(@Field("email") String email, @Field("password") String password);

        @FormUrlEncoded
        @POST("/tournesia-rest/index.php/APIv1/tournesia/login")
        Call<APIUserData> login(@Field("email") String email, @Field("password") String password);

        @FormUrlEncoded
        @POST("/tournesia-rest/index.php/APIv1/tournesia/voteUp")
        Call<APIBaseResponse> voteUp(@Field("email") String email, @Field("id_wisata") String id_wisata);

        @FormUrlEncoded
        @POST("/tournesia-rest/index.php/APIv1/tournesia/voteNormal")
        Call<APIBaseResponse> voteNormal(@Field("email") String email, @Field("id_wisata") String id_wisata);

        @FormUrlEncoded
        @POST("/tournesia-rest/index.php/APIv1/tournesia/redeemChallenge")
        Call<APIUserData> redeemChallenge(@Field("email") String email, @Field("challengeno") String challengeno, @Field("addmedal") String addmedal);

        @FormUrlEncoded
        @POST("/tournesia-rest/index.php/APIv1/tournesia/redeemRewards")
        Call<APIUserData> redeemRewards(@Field("email") String email, @Field("minusmedal") String minusmedal, @Field("namahadiah") String hadiah);

        @FormUrlEncoded
        @POST("/tournesia-rest/index.php/APIv1/tournesia/addKonten ")
        Call<APIBaseResponse> addKonten(@Field("email") String email, @Field("nama") String nama, @Field("detail") String detail, @Field("judul") String judul
                , @Field("gambar") String gambar, @Field("tags") String tags, @Field("lokasi") String lokasi, @Field("namagambar") String namagambar);

    }

}



