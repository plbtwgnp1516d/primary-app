package com.plbtw.tournesia.Adapter;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.plbtw.tournesia.Activity.doRedeem;
import com.plbtw.tournesia.Model.Challenge;
import com.plbtw.tournesia.R;

import java.util.ArrayList;

/**
 * Created by 高橋六羽 on 5/31/2016.
 */
public class PoinAdapter  extends ArrayAdapter<Challenge>{
    Context context;
    int resLayout;
    ArrayList<Challenge> listChallenge;

    TextView tvChallenge;
    TextView tvPoin;
    Challenge navLisChallenge;
    ImageView logoPoin;
    private doRedeem listener;

    public PoinAdapter(Context context, int resLayout, ArrayList<Challenge> listChallenge, doRedeem listener){
        super(context, resLayout, listChallenge);
        this.context=context;
        this.resLayout=resLayout;
        this.listChallenge=listChallenge;
        this.listener = listener;
    }

    @Override
    public View getView(int position, final View convertView, ViewGroup parent) {

        View v = View.inflate(context,resLayout,null);

        tvChallenge = (TextView)v.findViewById(R.id.txtChallenge);
        tvPoin = (TextView)v.findViewById(R.id.txtPoin);
        logoPoin = (ImageView)v.findViewById(R.id.imgPoin);

        navLisChallenge = listChallenge.get(position);

        tvChallenge.setText(navLisChallenge.getChallengetext());

        if(navLisChallenge.isClear())
        {
            logoPoin.setVisibility(View.GONE);
            tvPoin.setText("SELESAI");
            tvPoin.setTextSize(10);
        }
        else {
            logoPoin.setVisibility(View.VISIBLE);
            tvPoin.setText(navLisChallenge.getChallengepoin());
            tvPoin.setTextSize(15);
        }

        tvPoin.setOnClickListener(new View.OnClickListener() {
              @Override
              public void onClick(View v) {
                  View parentRow = (View) v.getParent();
                  ListView listView = (ListView) parentRow.getParent();
                  final int position = listView.getPositionForView((View) v.getParent());
                  navLisChallenge = listChallenge.get(position);
                  if(!navLisChallenge.isClear())
                  {
                      listener.redeemChallenge(navLisChallenge.getChallenge_id(), navLisChallenge.getChallengepoin());
                  }
                  else if(navLisChallenge.isClear())
                  {
                      Toast.makeText(context, "Misi telah selesai", Toast.LENGTH_SHORT).show();
                  }
              }
          }
        );

        return v;
    }
}
