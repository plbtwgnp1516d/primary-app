package com.plbtw.tournesia.Model;

import java.io.Serializable;

/**
 * Created by 高橋六羽 on 6/1/2016.
 */
@SuppressWarnings("serial")
public class Hadiah implements Serializable{
    int ImageHadiah;
    String TextHadiah, TextDetail, harga;

    public Hadiah(int imageHadiah, String textHadiah, String textDetail, String harga) {
        ImageHadiah = imageHadiah;
        TextHadiah = textHadiah;
        TextDetail = textDetail;
        this.harga = harga;
    }

    public String getHarga() {
        return harga;
    }

    public void setHarga(String harga) {
        this.harga = harga;
    }

    public int getImageHadiah() {

        return ImageHadiah;
    }

    public void setImageHadiah(int imageHadiah) {
        ImageHadiah = imageHadiah;
    }

    public String getTextHadiah() {
        return TextHadiah;
    }

    public void setTextHadiah(String textHadiah) {
        TextHadiah = textHadiah;
    }

    public String getTextDetail() {
        return TextDetail;
    }

    public void setTextDetail(String textDetail) {
        TextDetail = textDetail;
    }
}
