package com.plbtw.tournesia.Model;

import java.io.Serializable;
import java.util.List;

/**
 * Created by 高橋六羽 on 5/24/2016.
 */
@SuppressWarnings("serial")
public class APIUserData extends APIBaseResponse implements Serializable{
    List<UserData> userData;

    public List<UserData> getUserData() {
        return userData;
    }

    public void setUserData(List<UserData> userData) {
        this.userData = userData;
    }
}
