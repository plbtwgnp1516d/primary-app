package com.plbtw.tournesia.Activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.plbtw.tournesia.Model.KontenData;
import com.plbtw.tournesia.R;

public class DetailActivity extends AppCompatActivity{

    ImageView DetailImage;
    TextView txtNamaDetail;
    TextView txtLokasiDetail;
    TextView txtDetailDetail;
    TextView txtTagsDetail;
    TextView txtTanggalDetail;
    private KontenData data;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        Intent i = getIntent();
        data = (KontenData) i.getSerializableExtra("KontenDataItem");

        DetailImage = (ImageView) findViewById(R.id.imgDetail);
        txtNamaDetail = (TextView) findViewById(R.id.txtNamaDetail);
        txtLokasiDetail = (TextView) findViewById(R.id.txtLokasiDetail);
        txtDetailDetail = (TextView) findViewById(R.id.txtDetailDetail);
        txtTagsDetail = (TextView) findViewById(R.id.txtTagsDetail);
        txtTanggalDetail = (TextView) findViewById(R.id.txtTanggalDetail);

        Glide.with(this).load(data.getGambar_Wisata()).into(DetailImage);
        txtNamaDetail.setText(data.getNama_Wisata());
        txtLokasiDetail.setText(data.getLokasi());
        txtDetailDetail.setText(data.getDetail_Wisata());
        txtTagsDetail.setText("Tags : " + data.getTags());
        txtTanggalDetail.setText("Tanggal : " + data.getCreated_at());
    }
}
