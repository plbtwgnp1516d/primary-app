package com.plbtw.tournesia.Activity;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.graphics.Point;
import android.graphics.drawable.BitmapDrawable;
import android.media.MediaScannerConnection;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.content.CursorLoader;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Base64;
import android.util.Log;
import android.view.Display;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.plbtw.tournesia.Model.APIBaseResponse;
import com.plbtw.tournesia.Preferences.SessionManager;
import com.plbtw.tournesia.R;
import com.plbtw.tournesia.Rest.RestClient;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import retrofit.Call;
import retrofit.Callback;
import retrofit.Response;

public class UploadActivity extends AppCompatActivity implements View.OnClickListener{

    private Toolbar toolbar;
    private SessionManager sessions;

    EditText etxtJudul;
    EditText etxtNama;
    EditText etxtLokasi;
    EditText etxtTags;
    EditText etxtDetail;
    ImageView AttachImage;
    Button bTambah;

    private Call<APIBaseResponse> callTambah;
    private RestClient.GitApiInterface service;

    public static int COUNT_CAMERA = 100;
    public static final int MEDIA_TYPE_IMAGE = 1;
    private static final String IMAGE_DIRECTORY_NAME = "Tournesia_CapturedPhoto";
    private Uri fileUri;

    private String timeStamp;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_upload);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        this.setSupportActionBar(toolbar);

        sessions = new SessionManager(this);

        etxtJudul = (EditText) findViewById(R.id.etJudul);
        etxtNama = (EditText) findViewById(R.id.etNama);
        etxtLokasi = (EditText) findViewById(R.id.etLokasi);
        etxtTags = (EditText) findViewById(R.id.etTags);
        etxtDetail = (EditText) findViewById(R.id.etDetail);
        AttachImage = (ImageView) findViewById(R.id.imgAttach);
        bTambah = (Button) findViewById(R.id.btnTambah);

        bTambah.setOnClickListener(this);
        AttachImage.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
        if(v==bTambah)
        {
            if (AttachImage.getDrawable() == null) {
                Toast.makeText(getApplicationContext(), "Silahkan tambahan foto terlebih dahulu", Toast.LENGTH_SHORT).show();
            } else {
                String photoName;
                generateTimeStamp();

                ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                ((BitmapDrawable) AttachImage.getDrawable()).getBitmap().compress(Bitmap.CompressFormat.JPEG, 50, byteArrayOutputStream);
                String encodedImage = Base64.encodeToString(byteArrayOutputStream.toByteArray(), Base64.DEFAULT);
                photoName= "konten_" + timeStamp +"_user" + sessions.getUserDetails().get(SessionManager.KEY_EMAIL)+ ".jpg";

                final ProgressDialog progressDialog = new ProgressDialog(UploadActivity.this,
                        R.style.ProgressDialog);
                progressDialog.setIndeterminate(true);
                progressDialog.setMessage("Menambah Konten...");
                progressDialog.show();

                // TODO: Implement your own authentication logic here.

                service = RestClient.getClient();
                callTambah = service.addKonten(sessions.getUserDetails().get(SessionManager.KEY_EMAIL), etxtNama.getText().toString(),
                        etxtDetail.getText().toString(), etxtJudul.getText().toString(), encodedImage, etxtTags.getText().toString(), etxtLokasi.getText().toString(), photoName);

                callTambah.enqueue(new Callback<APIBaseResponse>() {
                    @Override
                    public void onResponse(Response<APIBaseResponse> response) {
                        Log.d("UploadActivity", "Status Code = " + response.code());
                        if (response.isSuccess()) {
                            // request successful (status code 200, 201)
                            APIBaseResponse result = response.body();
                            Log.d("UploadActivity", "response = " + new Gson().toJson(result));
                            if (result != null) {
                                progressDialog.dismiss();
                                Toast.makeText(getApplicationContext(), "Konten berhasil ditambahkan",
                                        Toast.LENGTH_SHORT).show();
                                finish();
                            }

                        } else {
                            // response received but request not successful (like 400,401,403 etc)
                            //Handle errors
                            progressDialog.dismiss();
                            Toast.makeText(getApplicationContext(), "Gagal menambahkan konten",
                                    Toast.LENGTH_SHORT).show();
                            finish();
                        }
                    }

                    @Override
                    public void onFailure(Throwable t) {
                        progressDialog.dismiss();
                        Toast.makeText(getApplicationContext(), "Gagal menambahkan konten",
                                Toast.LENGTH_SHORT).show();
                        finish();
                    }
                });
            }

        }
        else if(v==AttachImage)
        {
            selectImage();
        }
    }

    private void selectImage() {
        final CharSequence[] items = { "Take Photo", "Choose from Library", "Cancel" };

        AlertDialog.Builder builder = new AlertDialog.Builder(UploadActivity.this);
        builder.setTitle("Add Photo!");
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                if (items[item].equals("Take Photo")) {
                    Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                    fileUri = getOutputMediaFileUri(MEDIA_TYPE_IMAGE);
                    intent.putExtra(MediaStore.EXTRA_OUTPUT, fileUri);
                    startActivityForResult(intent, COUNT_CAMERA);
                } else if (items[item].equals("Choose from Library")) {
                    Intent intent = new Intent(
                            Intent.ACTION_PICK,
                            android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                    intent.setType("image/*");
                    startActivityForResult(
                            Intent.createChooser(intent, "Select File"),
                            2);
                } else if (items[item].equals("Cancel")) {
                    dialog.dismiss();
                }
            }
        });
        builder.show();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            if (requestCode == COUNT_CAMERA) {

                Bitmap bitmap;
                BitmapFactory.Options options = new BitmapFactory.Options();
                options.inJustDecodeBounds = true;
                BitmapFactory.decodeFile(fileUri.getPath(), options);
                final int REQUIRED_SIZE = 450;
                int scale = 1;
                while (options.outWidth / scale / 2 >= REQUIRED_SIZE
                        && options.outHeight / scale / 2 >= REQUIRED_SIZE)
                    scale *= 2;
                options.inSampleSize = scale;
                options.inJustDecodeBounds = false;
                bitmap =  BitmapFactory.decodeFile(fileUri.getPath(), options);

                AttachImage.setImageBitmap(bitmap);
                AttachImage.setScaleType(ImageView.ScaleType.FIT_XY);

            } else if (requestCode == 2) {
                Uri selectedImageUri = data.getData();
                String[] projection = {MediaStore.MediaColumns.DATA};
                CursorLoader cursorLoader = new CursorLoader(this, selectedImageUri, projection, null, null,
                        null);
                Cursor cursor = cursorLoader.loadInBackground();
                int column_index = cursor.getColumnIndexOrThrow(MediaStore.MediaColumns.DATA);
                cursor.moveToFirst();

                String selectedImagePath = cursor.getString(column_index);

                Bitmap bm;
                BitmapFactory.Options options = new BitmapFactory.Options();
                options.inJustDecodeBounds = true;
                BitmapFactory.decodeFile(selectedImagePath, options);
                final int REQUIRED_SIZE = 450;
                int scale = 1;
                while (options.outWidth / scale / 2 >= REQUIRED_SIZE
                        && options.outHeight / scale / 2 >= REQUIRED_SIZE)
                    scale *= 2;
                options.inSampleSize = scale;
                options.inJustDecodeBounds = false;
                bm = BitmapFactory.decodeFile(selectedImagePath, options);

                Display display = getWindowManager().getDefaultDisplay();
                Point size = new Point();
                display.getSize(size);
                int width = size.x;

                if(bm.getWidth()<width){
                    int heighttoAdd = bm.getHeight()+(width-bm.getWidth());
                    Bitmap newResizedBitmap = getResizedBitmap(bm,width,heighttoAdd);
                    AttachImage.setImageBitmap(newResizedBitmap);
                }else{
                    AttachImage.setImageBitmap(bm);
                    AttachImage.setScaleType(ImageView.ScaleType.FIT_START);
                }
            }
        }
    }

    public Bitmap getResizedBitmap(Bitmap bm, int newWidth, int newHeight) {
        int width = bm.getWidth();
        int height = bm.getHeight();
        float scaleWidth = ((float) newWidth) / width;
        float scaleHeight = ((float) newHeight) / height;
        // CREATE A MATRIX FOR THE MANIPULATION
        Matrix matrix = new Matrix();
        // RESIZE THE BIT MAP
        matrix.postScale(scaleWidth, scaleHeight);

        // "RECREATE" THE NEW BITMAP
        Bitmap resizedBitmap = Bitmap.createBitmap(
                bm, 0, 0, width, height, matrix, false);
        bm.recycle();
        return resizedBitmap;
    }

    public Uri getOutputMediaFileUri(int type) {
        return Uri.fromFile(getOutputMediaFile(type));
    }

    private File getOutputMediaFile(int type) {
        // External sdcard location
        File mediaStorageDir = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES), IMAGE_DIRECTORY_NAME);

        // Create the storage directory if it does not exist
        if (!mediaStorageDir.exists()) {
            if (!mediaStorageDir.mkdirs()) {
                return null;
            }
        }

        // Create a media file name
        File mediaFile;
        if (type == MEDIA_TYPE_IMAGE) {
            mediaFile = new File(mediaStorageDir.getPath() + File.separator + "IMG_" + timeStamp +"_user" + sessions.getUserDetails().get(SessionManager.KEY_EMAIL)+ ".jpg");
        } else {
            return null;
        }
        MediaScannerConnection.scanFile(this, new String[]{Environment.getExternalStorageDirectory().toString()},
                null, new MediaScannerConnection.OnScanCompletedListener() {
                    public void onScanCompleted(String path, Uri uri) {
                        Log.i("ExternalStorage", "Scanned " + path + ":");
                        Log.i("ExternalStorage", "-> uri=" + uri);
                    }
                });
        return mediaFile;
    }

    private void generateTimeStamp(){
        timeStamp= new SimpleDateFormat("yyyyMMdd_HHmmss", Locale.getDefault()).format(new Date());
    }
}
