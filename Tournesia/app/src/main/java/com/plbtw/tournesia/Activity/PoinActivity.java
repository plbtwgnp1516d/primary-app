package com.plbtw.tournesia.Activity;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.plbtw.tournesia.Adapter.PoinAdapter;
import com.plbtw.tournesia.Model.APIUserData;
import com.plbtw.tournesia.Model.APIVotes;
import com.plbtw.tournesia.Model.Challenge;
import com.plbtw.tournesia.Preferences.SessionManager;
import com.plbtw.tournesia.R;
import com.plbtw.tournesia.Rest.RestClient;

import java.util.ArrayList;

import retrofit.Call;
import retrofit.Callback;
import retrofit.Response;

public class PoinActivity extends AppCompatActivity implements doRedeem{

    private Toolbar toolbar;
    private ListView lvChallenge;
    private PoinAdapter poinAdapter;
    private ArrayList<Challenge> challengeItems = new ArrayList<Challenge>();

    private Call<APIVotes> callVotes;
    private Call<APIUserData> callRedeem;
    private RestClient.GitApiInterface service;

    private SessionManager sessions;

    private TextView tvJumlah;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_poin);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        this.setSupportActionBar(toolbar);

        sessions = new SessionManager(this);

        tvJumlah = (TextView) findViewById(R.id.txtJumlah);

        final ProgressDialog progressDialog = new ProgressDialog(PoinActivity.this,
                R.style.ProgressDialog);
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage("Menghitung Votes...");
        progressDialog.show();

        service = RestClient.getClient();
        callVotes = service.getVotes(sessions.getUserDetails().get(SessionManager.KEY_EMAIL));

        callVotes.enqueue(new Callback<APIVotes>() {
            @Override
            public void onResponse(Response<APIVotes> response) {
                Log.d("PoinActivity", "Status Code = " + response.code());
                if (response.isSuccess()) {
                    // request successful (status code 200, 201)
                    APIVotes result = response.body();
                    Log.d("PoinActivity", "response = " + new Gson().toJson(result));
                    if (result != null) {
                        tvJumlah.setText(String.valueOf(result.getTotalVotes()));
                        progressDialog.dismiss();
                    }

                } else {
                    // response received but request not successful (like 400,401,403 etc)
                    //Handle errors
                    progressDialog.dismiss();
                }
            }

            @Override
            public void onFailure(Throwable t) {
                progressDialog.dismiss();
            }
        });

        lvChallenge = (ListView) findViewById(R.id.listChallenge);

        poinAdapter = new PoinAdapter(getApplicationContext(), R.layout.item_challenge, challengeItems, this);

        lvChallenge.setAdapter(poinAdapter);

        refreshData();

    }

    private void refreshData()
    {
        sessions = new SessionManager(this);

        challengeItems.clear();
        Challenge C1 = new Challenge("1", "10 Votes", "5", !sessions.getUserDetails().get(SessionManager.KEY_C1).equals("0"));
        Challenge C2 = new Challenge("2", "25 Votes", "10", !sessions.getUserDetails().get(SessionManager.KEY_C2).equals("0"));
        Challenge C3 = new Challenge("3", "50 Votes", "25", !sessions.getUserDetails().get(SessionManager.KEY_C3).equals("0"));
        Challenge C4 = new Challenge("4", "75 Votes", "50", !sessions.getUserDetails().get(SessionManager.KEY_C4).equals("0"));
        Challenge C5 = new Challenge("5", "100 Votes", "100", !sessions.getUserDetails().get(SessionManager.KEY_C5).equals("0"));

        challengeItems.add(C1);
        challengeItems.add(C2);
        challengeItems.add(C3);
        challengeItems.add(C4);
        challengeItems.add(C5);
        poinAdapter.notifyDataSetChanged();
    }

    public void redeemChallenge(String challengeno, String poin)
    {
        if(challengeno.equalsIgnoreCase("1"))
        {
            if(Integer.valueOf(tvJumlah.getText().toString())<10)
                Toast.makeText(getApplicationContext(), "Jumlah votes tidak mencukupi", Toast.LENGTH_SHORT).show();
            else
                finishChallenge(challengeno,poin);
        }
        else if(challengeno.equalsIgnoreCase("2"))
        {
            if(Integer.valueOf(tvJumlah.getText().toString())<25)
                Toast.makeText(getApplicationContext(), "Jumlah votes tidak mencukupi", Toast.LENGTH_SHORT).show();
            else
                finishChallenge(challengeno, poin);
        }
        else if(challengeno.equalsIgnoreCase("3"))
        {
            if(Integer.valueOf(tvJumlah.getText().toString())<50)
                Toast.makeText(getApplicationContext(), "Jumlah votes tidak mencukupi", Toast.LENGTH_SHORT).show();
            else
                finishChallenge(challengeno, poin);
        }
        else if(challengeno.equalsIgnoreCase("4"))
        {
            if(Integer.valueOf(tvJumlah.getText().toString())<75)
                Toast.makeText(getApplicationContext(), "Jumlah votes tidak mencukupi", Toast.LENGTH_SHORT).show();
            else
                finishChallenge(challengeno, poin);
        }
        else if(challengeno.equalsIgnoreCase("5"))
        {
            if(Integer.valueOf(tvJumlah.getText().toString())<100)
                Toast.makeText(getApplicationContext(), "Jumlah votes tidak mencukupi", Toast.LENGTH_SHORT).show();
            else
                finishChallenge(challengeno,poin);
        }
    }

    private void finishChallenge(String challengeno, String poin)
    {
        final ProgressDialog progressDialog = new ProgressDialog(PoinActivity.this,
                R.style.ProgressDialog);
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage("Menukar Poin...");
        progressDialog.show();

        service = RestClient.getClient();
        callRedeem = service.redeemChallenge(sessions.getUserDetails().get(SessionManager.KEY_EMAIL), challengeno, poin);

        callRedeem.enqueue(new Callback<APIUserData>() {
            @Override
            public void onResponse(Response<APIUserData> response) {
                Log.d("PoinActivity", "Status Code = " + response.code());
                if (response.isSuccess()) {
                    // request successful (status code 200, 201)
                    APIUserData result = response.body();
                    Log.d("PoinActivity", "response = " + new Gson().toJson(result));
                    if (result != null) {
                        sessions.createLoginSession(result.getUserData().get(0));
                        refreshData();
                        progressDialog.dismiss();
                    }

                } else {
                    // response received but request not successful (like 400,401,403 etc)
                    //Handle errors
                    progressDialog.dismiss();
                }
            }

            @Override
            public void onFailure(Throwable t) {
                progressDialog.dismiss();
            }
        });
    }



}
