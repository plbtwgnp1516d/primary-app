package com.plbtw.tournesia.Model;

import java.io.Serializable;

/**
 * Created by 高橋六羽 on 5/31/2016.
 */
@SuppressWarnings("serial")
public class Challenge implements Serializable{
    String challenge_id, challengetext, challengepoin;
    boolean clear;

    public boolean isClear() {
        return clear;
    }

    public void setClear(boolean clear) {
        this.clear = clear;
    }

    public String getChallenge_id() {
        return challenge_id;
    }

    public void setChallenge_id(String challenge_id) {
        this.challenge_id = challenge_id;
    }

    public String getChallengetext() {
        return challengetext;
    }

    public void setChallengetext(String challengetext) {
        this.challengetext = challengetext;
    }

    public Challenge(String challenge_id, String challengetext, String challengepoin, boolean clear) {
        this.challenge_id = challenge_id;
        this.challengetext = challengetext;
        this.challengepoin = challengepoin;
        this.clear = clear;
    }

    public String getChallengepoin() {
        return challengepoin;
    }

    public void setChallengepoin(String challengepoin) {
        this.challengepoin = challengepoin;
    }
}
