package com.plbtw.tournesia.Model;

import java.io.Serializable;

/**
 * Created by 高橋六羽 on 5/24/2016.
 */
@SuppressWarnings("serial")
public class UserData implements Serializable{
    String ID_UserApp;
    String Email;
    String Password;
    String Jumlah_Medal;
    String Challenge1,Challenge2,Challenge3,Challenge4,Challenge5;

    public String getID_UserApp() {
        return ID_UserApp;
    }

    public void setID_UserApp(String ID_UserApp) {
        this.ID_UserApp = ID_UserApp;
    }

    public String getEmail() {
        return Email;
    }

    public void setEmail(String email) {
        Email = email;
    }

    public String getPassword() {
        return Password;
    }

    public void setPassword(String password) {
        Password = password;
    }

    public String getJumlah_Medal() {
        return Jumlah_Medal;
    }

    public void setJumlah_Medal(String jumlah_Medal) {
        Jumlah_Medal = jumlah_Medal;
    }

    public String getChallenge1() {
        return Challenge1;
    }

    public void setChallenge1(String challenge1) {
        Challenge1 = challenge1;
    }

    public String getChallenge2() {
        return Challenge2;
    }

    public void setChallenge2(String challenge2) {
        Challenge2 = challenge2;
    }

    public String getChallenge3() {
        return Challenge3;
    }

    public void setChallenge3(String challenge3) {
        Challenge3 = challenge3;
    }

    public String getChallenge4() {
        return Challenge4;
    }

    public void setChallenge4(String challenge4) {
        Challenge4 = challenge4;
    }

    public String getChallenge5() {
        return Challenge5;
    }

    public void setChallenge5(String challenge5) {
        Challenge5 = challenge5;
    }
}
