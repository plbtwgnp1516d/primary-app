package com.plbtw.tournesia.Model;

import java.io.Serializable;

/**
 * Created by 高橋六羽 on 5/30/2016.
 */
@SuppressWarnings("serial")
public class APIVotes extends APIBaseResponse implements Serializable{
    int totalVotes;

    public int getTotalVotes() {
        return totalVotes;
    }

    public void setTotalVotes(int totalVotes) {
        this.totalVotes = totalVotes;
    }
}
