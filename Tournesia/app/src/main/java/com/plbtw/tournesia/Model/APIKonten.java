package com.plbtw.tournesia.Model;

import java.io.Serializable;
import java.util.List;

/**
 * Created by 高橋六羽 on 5/24/2016.
 */
@SuppressWarnings("serial")
public class APIKonten extends APIBaseResponse implements Serializable{
    List<KontenData> kontenData;

    public List<KontenData> getKontenData() {
        return kontenData;
    }

    public void setKontenData(List<KontenData> kontenData) {
        this.kontenData = kontenData;
    }
}
