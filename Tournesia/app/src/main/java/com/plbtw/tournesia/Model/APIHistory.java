package com.plbtw.tournesia.Model;

import java.util.List;

/**
 * Created by 高橋六羽 on 6/7/2016.
 */
public class APIHistory extends APIBaseResponse {
    List<History> historyData;

    public List<History> getHistoryData() {
        return historyData;
    }

    public void setHistoryData(List<History> historyData) {
        this.historyData = historyData;
    }
}
