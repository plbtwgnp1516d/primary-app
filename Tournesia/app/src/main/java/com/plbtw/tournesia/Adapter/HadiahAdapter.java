package com.plbtw.tournesia.Adapter;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.plbtw.tournesia.Activity.doTukar;
import com.plbtw.tournesia.Model.Hadiah;
import com.plbtw.tournesia.R;

import java.util.ArrayList;

/**
 * Created by 高橋六羽 on 6/1/2016.
 */
public class HadiahAdapter extends ArrayAdapter<Hadiah> {
    Context context;
    int resLayout;
    ArrayList<Hadiah> listHadiah;

    TextView tvHadiah;
    TextView tvDetail;
    TextView tvHarga;
    Hadiah navLisHadiah;
    ImageView imgHadiah;
    Button btnTukar;
    private doTukar listener;

    public HadiahAdapter(Context context, int resLayout, ArrayList<Hadiah> listHadiah, doTukar listener){
        super(context, resLayout, listHadiah);
        this.context=context;
        this.resLayout=resLayout;
        this.listHadiah=listHadiah;
        this.listener = listener;
    }

    @Override
    public View getView(int position, final View convertView, ViewGroup parent) {

        View v = View.inflate(context,resLayout,null);

        tvHadiah = (TextView)v.findViewById(R.id.txtHadiahHistory);
        tvDetail = (TextView)v.findViewById(R.id.txtDetailHistory);
        tvHarga = (TextView)v.findViewById(R.id.txtHarga);
        imgHadiah = (ImageView)v.findViewById(R.id.imgHadiahHistory);
        btnTukar = (Button)v.findViewById(R.id.btnRedeem);

        navLisHadiah = listHadiah.get(position);

        tvHadiah.setText(navLisHadiah.getTextHadiah());
        tvDetail.setText(navLisHadiah.getTextDetail());
        tvHarga.setText(navLisHadiah.getHarga());
        imgHadiah.setImageResource(navLisHadiah.getImageHadiah());

        btnTukar.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {
                                            View parentRow = (View) v.getParent();
                                            ListView listView = (ListView) parentRow.getParent();
                                            final int position = listView.getPositionForView((View) v.getParent());
                                            navLisHadiah = listHadiah.get(position);

                                            listener.redeemReward(navLisHadiah.getHarga(), navLisHadiah.getTextHadiah());

                                        }
                                    }
        );

        return v;
    }
}
