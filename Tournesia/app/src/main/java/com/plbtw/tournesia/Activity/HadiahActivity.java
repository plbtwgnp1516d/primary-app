package com.plbtw.tournesia.Activity;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.plbtw.tournesia.Adapter.HadiahAdapter;
import com.plbtw.tournesia.Model.APIUserData;
import com.plbtw.tournesia.Model.APIVotes;
import com.plbtw.tournesia.Model.Hadiah;
import com.plbtw.tournesia.Preferences.SessionManager;
import com.plbtw.tournesia.R;
import com.plbtw.tournesia.Rest.RestClient;

import java.util.ArrayList;

import retrofit.Call;
import retrofit.Callback;
import retrofit.Response;

/**
 * Created by 高橋六羽 on 5/30/2016.
 */
public class HadiahActivity extends AppCompatActivity implements doTukar{

    private Toolbar toolbar;
    private ListView lvHadiah;
    private HadiahAdapter hadiahAdapter;
    private ArrayList<Hadiah> hadiahItems = new ArrayList<Hadiah>();

    private Call<APIVotes> callVotes;
    private Call<APIUserData> callRedeem;
    private RestClient.GitApiInterface service;

    private SessionManager sessions;

    private TextView tvJumlahPoin;

    public HadiahActivity() {
        // Required empty public constructor
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_hadiah);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        this.setSupportActionBar(toolbar);

        sessions = new SessionManager(this);

        tvJumlahPoin = (TextView) findViewById(R.id.txtJumlahPoin);

        lvHadiah = (ListView) findViewById(R.id.listViewHadiah);

        hadiahAdapter = new HadiahAdapter(getApplicationContext(), R.layout.item_hadiah, hadiahItems, this);

        lvHadiah.setAdapter(hadiahAdapter);

        refreshData();
    }

    private void refreshData()
    {
        sessions = new SessionManager(this);

        hadiahItems.clear();
        Hadiah H1 = new Hadiah(R.drawable.tas, "Tas Travel", "Tas cocok untuk berpergian", "15");
        Hadiah H2 = new Hadiah(R.drawable.tiket, "Tiket Pesawat", "Tiket pulang pergi wilayah Indonesia", "80");
        Hadiah H3 = new Hadiah(R.drawable.liburan, "Liburan", "2 Hari 3 Malam wilayah Indonesia", "190");

        hadiahItems.add(H1);
        hadiahItems.add(H2);
        hadiahItems.add(H3);

        tvJumlahPoin.setText(sessions.getUserDetails().get(SessionManager.KEY_MEDAL));

        hadiahAdapter.notifyDataSetChanged();
    }

    public void redeemReward(String minusmedal, String namahadiah)
    {
        if(Integer.valueOf(minusmedal)>Integer.valueOf(sessions.getUserDetails().get(SessionManager.KEY_MEDAL)))
        {
            Toast.makeText(getApplicationContext(), "Anda tidak memiliki poin yang cukup", Toast.LENGTH_SHORT).show();
        }
        else {
            final ProgressDialog progressDialog = new ProgressDialog(HadiahActivity.this,
                    R.style.ProgressDialog);
            progressDialog.setIndeterminate(true);
            progressDialog.setMessage("Menukar Hadiah...");
            progressDialog.show();

            service = RestClient.getClient();
            callRedeem = service.redeemRewards(sessions.getUserDetails().get(SessionManager.KEY_EMAIL), minusmedal, namahadiah);

            callRedeem.enqueue(new Callback<APIUserData>() {
                @Override
                public void onResponse(Response<APIUserData> response) {
                    Log.d("HadiahActivity", "Status Code = " + response.code());
                    if (response.isSuccess()) {
                        // request successful (status code 200, 201)
                        APIUserData result = response.body();
                        Log.d("HadiahActivity", "response = " + new Gson().toJson(result));
                        if (result != null) {
                            sessions.createLoginSession(result.getUserData().get(0));
                            refreshData();
                            Toast.makeText(getApplicationContext(), "Penukaran Hadiah berhasil", Toast.LENGTH_SHORT).show();
                            progressDialog.dismiss();
                        }

                    } else {
                        // response received but request not successful (like 400,401,403 etc)
                        //Handle errors
                        progressDialog.dismiss();
                    }
                }

                @Override
                public void onFailure(Throwable t) {
                    progressDialog.dismiss();
                }
            });
        }
    }
}
