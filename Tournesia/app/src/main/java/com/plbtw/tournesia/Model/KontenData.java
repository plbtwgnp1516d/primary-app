package com.plbtw.tournesia.Model;

import java.io.Serializable;
import java.util.List;

/**
 * Created by 高橋六羽 on 5/24/2016.
 */
@SuppressWarnings("serial")
public class KontenData implements Serializable{
    String ID_Wisata;
    String Email;
    String Nama_Wisata;
    String Detail_Wisata;
    String Judul_Konten;

    public String getJudul_Konten() {
        return Judul_Konten;
    }

    public void setJudul_Konten(String judul_Konten) {
        Judul_Konten = judul_Konten;
    }

    String Tipe_Wisata;
    String Gambar_Wisata;
    String Tags;
    String Lokasi;
    String Created_at;
    int Jumlah_Votes;
    List<Flags> Flag;
    int state;

    public int getState() {
        return state;
    }

    public void setState(int state) {
        this.state = state;
    }

    public List<Flags> getFlag() {
        return Flag;
    }

    public String getID_Wisata() {
        return ID_Wisata;
    }

    public void setID_Wisata(String ID_Wisata) {
        this.ID_Wisata = ID_Wisata;
    }

    public String getCreated_at() {
        return Created_at;
    }

    public void setCreated_at(String created_at) {
        Created_at = created_at;
    }

    public void setFlag(List<Flags> flag) {
        Flag = flag;
    }


    public String getEmail() {
        return Email;
    }

    public void setEmail(String email) {
        Email = email;
    }

    public String getNama_Wisata() {
        return Nama_Wisata;
    }

    public void setNama_Wisata(String nama_Wisata) {
        Nama_Wisata = nama_Wisata;
    }

    public String getDetail_Wisata() {
        return Detail_Wisata;
    }

    public void setDetail_Wisata(String detail_Wisata) {
        Detail_Wisata = detail_Wisata;
    }

    public String getTipe_Wisata() {
        return Tipe_Wisata;
    }

    public void setTipe_Wisata(String tipe_Wisata) {
        Tipe_Wisata = tipe_Wisata;
    }

    public String getGambar_Wisata() {
        return Gambar_Wisata;
    }

    public void setGambar_Wisata(String gambar_Wisata) {
        Gambar_Wisata = gambar_Wisata;
    }

    public int getJumlah_Votes() {
        return Jumlah_Votes;
    }

    public void setJumlah_Votes(int jumlah_Votes) {
        Jumlah_Votes = jumlah_Votes;
    }

    public String getTags() {
        return Tags;
    }

    public void setTags(String tags) {
        Tags = tags;
    }

    public String getLokasi() {
        return Lokasi;
    }

    public void setLokasi(String lokasi) {
        Lokasi = lokasi;
    }

}
