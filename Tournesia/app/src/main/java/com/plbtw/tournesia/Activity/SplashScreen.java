package com.plbtw.tournesia.Activity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.Toast;

import com.google.gson.Gson;
import com.plbtw.tournesia.Model.APIUserData;
import com.plbtw.tournesia.Model.UserData;
import com.plbtw.tournesia.Fragment.NewFragment;
import com.plbtw.tournesia.Preferences.SessionManager;
import com.plbtw.tournesia.R;
import com.plbtw.tournesia.Rest.RestClient;

import java.util.HashMap;

import retrofit.Call;
import retrofit.Callback;
import retrofit.Response;

public class SplashScreen extends AppCompatActivity {

    private SessionManager sessions;
    UserData loginuser;

    private Call<APIUserData> callLogin;
    private RestClient.GitApiInterface service;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);

        sessions = new SessionManager(this);

        new Handler().postDelayed(new Runnable() {

            /*
             * Showing splash screen with a timer. This will be useful when you
             * want to show case your app logo / company
             */

            @Override
            public void run() {
                // This method will be executed once the timer is over
                // Start your app main activity

                if (!sessions.isLoggedIn()) {
                    Intent i = new Intent(SplashScreen.this, LoginActivity.class);
                    startActivity(i);
                } else {
                    getNewData();
                }

            }
        }, 0);
    }

    public void getNewData()
    {
        HashMap<String,String> userdata = sessions.getUserDetails();

        service = RestClient.getClient();
        callLogin = service.login(userdata.get(SessionManager.KEY_EMAIL), userdata.get(SessionManager.KEY_PASSWORD));

        callLogin.enqueue(new Callback<APIUserData>() {
            @Override
            public void onResponse(Response<APIUserData> response) {
                Log.d("Splash Screen", "Status Code = " + response.code());
                if (response.isSuccess()) {
                    // request successful (status code 200, 201)
                    APIUserData result = response.body();
                    Log.d("Splash Screen", "response = " + new Gson().toJson(result));
                    if (result != null) {
                        sessions.createLoginSession(result.getUserData().get(0));
                        Intent i = new Intent(SplashScreen.this, MainActivity.class);
                        startActivity(i);
                    }

                } else {
                    // response received but request not successful (like 400,401,403 etc)
                    //Handle errors
                    Toast.makeText(getApplicationContext(), "Koneksi Ke Internet Gagal", Toast.LENGTH_SHORT).show();
                    Intent i = new Intent(SplashScreen.this, LoginActivity.class);
                    startActivity(i);
                }
            }

            @Override
            public void onFailure(Throwable t) {
                Toast.makeText(getApplicationContext(), "Koneksi Ke Internet Gagal", Toast.LENGTH_SHORT).show();
                Intent i = new Intent(SplashScreen.this, LoginActivity.class);
                startActivity(i);
            }
        });
    }
}
