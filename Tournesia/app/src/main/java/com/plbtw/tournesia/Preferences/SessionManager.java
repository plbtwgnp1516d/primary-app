package com.plbtw.tournesia.Preferences;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;

import com.plbtw.tournesia.Activity.LoginActivity;
import com.plbtw.tournesia.Model.UserData;

import java.util.HashMap;

/**
 * Created by 高橋六羽 on 2016/03/11.
 */
public class SessionManager {
    SharedPreferences pref;

    // Editor for Shared preferences
    SharedPreferences.Editor editor;

    // Context
    Context _context;

    // Shared pref mode
    int PRIVATE_MODE = 0;

    // Sharedpref file name
    private static final String PREF_NAME = "TournesiaPref";

    // All Shared Preferences Keys

    //START USER-DATA
    private static final String IS_LOGIN = "IsLoggedIn";
    public static final String KEY_USERID = "user_id";
    public static final String KEY_EMAIL = "email";
    public static final String KEY_PASSWORD = "password";
    public static final String KEY_MEDAL = "medal";
    public static final String KEY_C1 = "c1";
    public static final String KEY_C2 = "c2";
    public static final String KEY_C3 = "c3";
    public static final String KEY_C4 = "c4";
    public static final String KEY_C5 = "c5";
    //END USER-DATA

    // Constructor
    public SessionManager(Context context) {
        this._context = context;
        pref = _context.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
        editor = pref.edit();
    }

    /**
     * Create login session
     */
    public void createLoginSession(UserData user) {
        // Storing login value as TRUE
        editor.putBoolean(IS_LOGIN, true);

        // Storing  in pref
        editor.putString(KEY_USERID, user.getID_UserApp());
        editor.putString(KEY_EMAIL, user.getEmail());
        editor.putString(KEY_PASSWORD, user.getPassword());
        editor.putString(KEY_MEDAL, user.getJumlah_Medal());
        editor.putString(KEY_C1, user.getChallenge1());
        editor.putString(KEY_C2, user.getChallenge2());
        editor.putString(KEY_C3, user.getChallenge3());
        editor.putString(KEY_C4, user.getChallenge4());
        editor.putString(KEY_C5, user.getChallenge5());

        // commit changes
        editor.commit();
    }

    /**
     * Check login method wil check user login status
     * If false it will redirect user to login page
     * Else won't do anything
     */
    public void checkLogin() {
        // Check login status
        if (!this.isLoggedIn()) {
            // user is not logged in redirect him to Login Activity
            Intent i = new Intent(_context, LoginActivity.class);
            // Closing all the Activities
            i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

            // Add new Flag to start new Activity
            i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

            // Staring Login Activity
            _context.startActivity(i);
        }
    }

    /**
     * Get stored session data
     */
    public HashMap<String, String> getUserDetails() {
        HashMap<String, String> user = new HashMap<String, String>();

        // user data
        user.put(KEY_USERID, pref.getString(KEY_USERID, null));
        user.put(KEY_EMAIL, pref.getString(KEY_EMAIL, null));
        user.put(KEY_PASSWORD, pref.getString(KEY_PASSWORD, null));
        user.put(KEY_MEDAL, pref.getString(KEY_MEDAL, null));
        user.put(KEY_C1, pref.getString(KEY_C1, null));
        user.put(KEY_C2, pref.getString(KEY_C2, null));
        user.put(KEY_C3, pref.getString(KEY_C3, null));
        user.put(KEY_C4, pref.getString(KEY_C4, null));
        user.put(KEY_C5, pref.getString(KEY_C5, null));

        // return user
        return user;
    }
    /**
     * Clear session details
     */
    public void logoutUser() {
        // Clearing all data from Shared Preferences
        editor.clear();
        editor.commit();

        // After logout redirect user to Loing Activity
        Intent i = new Intent(_context, LoginActivity.class);
        // Closing all the Activities
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);

        // Staring Login Activity
        _context.startActivity(i);
    }

    /**
     * Quick check for login
     **/
    // Get Login State
    public boolean isLoggedIn() {
        return pref.getBoolean(IS_LOGIN, false);
    }
}
