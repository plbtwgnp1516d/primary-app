package com.plbtw.tournesia.Activity;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.plbtw.tournesia.Adapter.HistoryAdapter;
import com.plbtw.tournesia.Model.APIHistory;
import com.plbtw.tournesia.Model.History;
import com.plbtw.tournesia.Preferences.SessionManager;
import com.plbtw.tournesia.R;
import com.plbtw.tournesia.Rest.RestClient;

import java.util.ArrayList;
import java.util.List;

import retrofit.Call;
import retrofit.Callback;
import retrofit.Response;

/**
 * Created by 高橋六羽 on 6/7/2016.
 */
public class HistoryActivity  extends AppCompatActivity {

    private Toolbar toolbar;
    private ListView lvHistory;
    private HistoryAdapter historyAdapter;
    private ArrayList<History> historyItems = new ArrayList<History>();

    private Call<APIHistory> callHistory;
    private RestClient.GitApiInterface service;

    private SessionManager sessions;

    private TextView tvJumlahPoin;

    public HistoryActivity() {
        // Required empty public constructor
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_history);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        this.setSupportActionBar(toolbar);

        sessions = new SessionManager(this);

        tvJumlahPoin = (TextView) findViewById(R.id.txtJumlahPoin);

        lvHistory = (ListView) findViewById(R.id.listViewHistory);

        historyAdapter = new HistoryAdapter(getApplicationContext(), R.layout.item_history, historyItems);

        lvHistory.setAdapter(historyAdapter);

        fetchData();

    }

    public void fetchData()
    {
        final ProgressDialog progressDialog = new ProgressDialog(HistoryActivity.this,
                R.style.ProgressDialog);
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage("Mengambil Riwayat...");
        progressDialog.show();

        service = RestClient.getClient();
        callHistory = service.getHistory(sessions.getUserDetails().get(SessionManager.KEY_EMAIL));
        callHistory.enqueue(new Callback<APIHistory>() {
            @Override
            public void onResponse(Response<APIHistory> response) {
                Log.d("HistoryActivity", "Status Code = " + response.code());
                if (response.isSuccess()) {
                    // request successful (status code 200, 201)
                    APIHistory result = response.body();
                    Log.d("HistoryActivity", "response = " + new Gson().toJson(result));
                    if (result != null) {

                        historyItems.clear();

                        List<History> historyResponseItems = result.getHistoryData();

                        if(historyResponseItems!=null)
                        {
                            for (History historyResponseItem : historyResponseItems) {
                                historyItems.add(historyResponseItem);
                                historyAdapter.notifyDataSetChanged();
                            }
                        }

                        progressDialog.dismiss();
                    }

                } else {
                    // response received but request not successful (like 400,401,403 etc)
                    //Handle errors
                    Toast.makeText(getApplicationContext(), "Koneksi Ke Internet Gagal", Toast.LENGTH_SHORT).show();
                    progressDialog.dismiss();
                }
            }

            @Override
            public void onFailure(Throwable t) {
                Toast.makeText(getApplicationContext(), "Koneksi Ke Internet Gagal", Toast.LENGTH_SHORT).show();
                progressDialog.dismiss();
            }
        });

    }


}
